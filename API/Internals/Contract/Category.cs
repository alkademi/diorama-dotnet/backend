using Diorama.Internals.Persistent.Models;

namespace Diorama.Internals.Contract;

public class CategoriesContract
{
    public IEnumerable<CategoryContract> Categories;

    public CategoriesContract(IEnumerable<Category> categories)
    {
        Categories = categories.Select<Category, CategoryContract>(x => new CategoryContract(x));
    }
}

public class CategoryContract
{
    public int ID { get; set; } = 0;
    public string Name { get; set; } = "";

    public CategoryContract(Category category)
    {
        ID = category.ID;
        Name = category.Name;
    }
}