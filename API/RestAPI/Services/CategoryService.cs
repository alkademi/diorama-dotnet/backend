using System.Net;
using Diorama.RestAPI.Repositories;
using Diorama.Internals.Contract;
using Diorama.Internals.Responses;
using Diorama.Internals.Persistent.Models;

namespace Diorama.RestAPI.Services;

public interface ICategoryService
{
    void GetCategoriesFromName(int userId, string name);
}

public class CategoryService : ICategoryService
{
    private ICategoryRepository _repo;
    private IUserRepository _userRepo;

    public CategoryService(ICategoryRepository repo, IUserRepository userRepo)
    {
        _repo = repo;
        _userRepo = userRepo;
    }

    public void GetCategoriesFromName(int userId, string name)
    {
        User? user = _userRepo.FindById(userId);
        if (user == null)
        {
            throw new ResponseError(HttpStatusCode.Conflict, "Data inconsistent.");
        }

        var categories = _repo.GetCategoriesByName(name);

        throw new ResponseOK(new CategoriesContract(categories));
    }
}